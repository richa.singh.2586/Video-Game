
public class Dancer extends Performer {

	private final String style;

	public Dancer(int unionId, String style) {

		super(unionId);
		this.style = style;

	}

	@Override
	public void perform() {

		System.out.println("Hi, I am  " + style + " " + unionId + " dancer");

	}

}
