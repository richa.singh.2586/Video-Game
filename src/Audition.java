
public class Audition {

	public static void main(String[] args) {

		// create an array of performers
		Performer[] performer = new Performer[7];
		
		// create 7 objects for performer and initialize them with their respective
		// union Id
		for (int i = 0; i <= 6; i++)
		{

			if (i <= 2) {
				performer[i] = new Performer(i);

			} else if (i <= 4) {
				performer[i] = new Dancer(i, "tap");
			} else if (i <= 5) {
				performer[i] = new Vocalist(i, 4);
			} else {

				performer[i] = new Vocalist(i, 3);
				((Vocalist) performer[i]).setVolume(4);
		}
		performer[i].perform();
		}
	}
}