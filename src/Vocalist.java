
public class Vocalist extends Performer {

	int key;
	int volume;

	public Vocalist(int unionId, int key) {
		super(unionId);
		this.key = key;

	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	@Override
	public void perform() {

		if (getVolume() > 0) {

			System.out.println("I sing in the key of " + key + " at the volume " + volume + "-" + unionId);

		} else {

			System.out.println("I sing in the key of " + key + "-" + unionId);

		}



	}

}
