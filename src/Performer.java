
public class Performer {

	protected final int unionId;

	public int getUnionId() {
		return unionId;
	}

	public Performer(int newUnionId) {
		unionId = newUnionId;
	}

	public void perform() {

		System.out.println("Hi, My Id is " + unionId + " - performer");

}
}